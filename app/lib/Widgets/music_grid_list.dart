import 'package:app/Classes/music.dart';
import 'package:flutter/material.dart';

class MusicGridList extends StatelessWidget {
  MusicGridList(this.musics, {Key key}) : super(key: key);

  final List<Music> musics;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: List.generate(musics.length, (index) {
        return Padding(
            padding: EdgeInsets.all(5),
            child: Image.asset(musics[index].cover));
      }),
    );
  }
}
