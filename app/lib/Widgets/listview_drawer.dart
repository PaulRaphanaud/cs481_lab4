import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListViewDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        Container(
          height: 250,
          child: DrawerHeader(
            decoration: BoxDecoration(color: Colors.blue),
            child: Column(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage('assets/George.png'),
                  radius: 50,
                ),
                Container(
                  color: Colors.white70,
                  padding: EdgeInsets.all(2),
                  child: Column(
                    children: [
                      Text('George Dupond'),
                      Text('Member since 2004; Gold Subscription'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.account_circle_rounded),
          title: Text('Account'),
          trailing: Icon(Icons.apps_rounded),
        ),
        ListTile(
          leading: Icon(Icons.audiotrack_rounded),
          title: Text('Music Store'),
          subtitle: Text('Browse new overpriced songs!'),
          trailing: Icon(Icons.forward),
        ),
        ListTile(
          leading: Icon(Icons.attribution_rounded),
          title: Text('Sign Out'),
          trailing: Icon(Icons.subdirectory_arrow_right_rounded),
        )
      ],
    );
  }
}
