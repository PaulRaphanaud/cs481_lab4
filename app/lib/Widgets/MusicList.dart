import 'package:app/Classes/music.dart';
import 'package:flutter/material.dart';

class MusicList extends StatelessWidget {
  MusicList(this.musics);

  final List<Music> musics;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: musics.length,
      itemBuilder: (context, idx) {
        return Card(
          child: ListTile(
            leading: Image.asset(musics[idx].cover),
            title: Text(musics[idx].title),
            subtitle: Row(
              children: [
                Text(musics[idx].artist),
                Text(" - " + musics[idx].album)
              ],
            ),
            trailing: Text(musics[idx].duration),
          ),
        );
      },
    );
  }
}
