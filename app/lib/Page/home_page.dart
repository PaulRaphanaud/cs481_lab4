import 'package:app/Widgets/listview_drawer.dart';
import 'package:app/Classes/music.dart';
import 'package:app/Widgets/MusicList.dart';
import 'package:app/Widgets/music_grid_list.dart';
import 'package:app/Classes/type_list.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Music> _musics = List<Music>();
  TypeList _selection;

  @override
  initState() {
    super.initState();
    _musics.add(Music(
        title: "Notorious",
        cover: "assets/images/malaa_notorious.jpg",
        album: "Notorious",
        artist: "Malaa",
        duration: "4:31"));
    _musics.add(Music(
        title: "Fire Drill",
        cover: "assets/images/mmartinez_firedrill.jpg",
        album: "EP Fire Drill",
        artist: "Melanie Martinez",
        duration: "4:15"));
    _musics.add(Music(
        title: "Delta",
        cover: "assets/images/delta.jpg",
        album: "EP Grand Place III",
        artist: "Honey&Badger",
        duration: "3:43"));
    _musics.add(Music(
        title: "Adieu",
        cover: "assets/images/adieu.jpg",
        album: "Revelations EP",
        artist: "Tchami",
        duration: "6:23"));
    _musics.add(Music(
        title: "Loverus",
        cover: "assets/images/tony.jpg",
        album: "EP Fire Drill",
        artist: "Tony Romera",
        duration: "4:22"));
    _musics.add(Music(
        title: "OCB",
        cover: "assets/images/ocb.jpg",
        album: "OCB",
        artist: "Malaa",
        duration: "4:15"));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(widget.title),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.person,
              color: Colors.white,
            ),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        actions: [
          PopupMenuButton<TypeList>(
            onSelected: (TypeList result) {
              setState(() {
                _selection = result;
              });
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<TypeList>>[
              const PopupMenuItem<TypeList>(
                value: TypeList.grid,
                child: Text('Grid View'),
              ),
              const PopupMenuItem<TypeList>(
                value: TypeList.list,
                child: Text('List View'),
              ),
            ],
          ),
        ],
      ),
      body: (_selection == TypeList.grid)
          ? MusicGridList(_musics)
          : MusicList(_musics),
      drawer: Drawer(
        child: ListViewDrawer(),
      ),
    );
  }
}
