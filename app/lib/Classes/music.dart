
import 'package:flutter/material.dart';

class Music
{
  String title;
  String cover;
  String album;
  String artist;
  String duration;

  Music({
    @required this.title,
    @required this.cover,
    this.album,
    @required this.artist,
    @required this.duration
  });
}